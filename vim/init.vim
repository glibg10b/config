let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin('~/.vim/plugged')

	source ~/.vim/common-plugins.vim
	Plug 'folke/tokyonight.nvim', { 'branch': 'main' }

call plug#end()

let g:tokyonight_style = "night"
let g:lightline = {'colorscheme': 'tokyonight'}
colorscheme tokyonight

source ~/.vim/vim.d/**/*.vim

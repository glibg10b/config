#!/bin/bash

# Absolute path
currentDir=$(cd $(dirname $0) && pwd)

rm -r ~/.config/btop
mkdir -p ~/.config/btop/
cp "$currentDir/btop.conf"  ~/.config/btop/btop.conf
cp -r "$currentDir/themes/" ~/.config/btop/

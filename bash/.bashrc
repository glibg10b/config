#!/bin/bash

# Head
#if [[ $- != *i* ]] ; then return; fi # Return if non-interactive
cmdExists() { command -v $1 >/dev/null; }

################################################################################
#                                   Aliases                                    #
################################################################################

if cmdExists bat;  then alias cat="bat";    fi
if cmdExists doas; then alias sudo="doas "; else alias sudo="sudo "; fi
if cmdExists lsd;  then alias ls="lsd";     fi
if cmdExists neovide && [[ -n "$DISPLAY" ]]
then
	alias vim="neovide --multigrid --"
elif cmdExists nvim; then alias vim="nvim"; fi

if cmdExists emerge; then
	alias e="emerge --ask --autounmask-backtrack=y --deep --tree --verbose \
		--unordered-display --jobs $(nproc) --load-average $(nproc)"

	alias e1="e --oneshot"
	alias e@="e @module-rebuild"
	alias eb="e --buildpkgonly"
	alias ec="e --depclean --verbose=n"
	alias ef="e --fetchonly"
	alias er="e --noreplace"
	alias es="e --sync"
	alias ese="e --search"
	alias eu="e --update @world"
	alias ew="e --deselect"

	if cmdExists equery; then
		alias eq="equery uses"
		alias eqd="equery depends"
		alias eqf="equery files"
		alias eqg="equery depgraph"
		alias eql="equery list"
	fi

	if cmdExists genlop; then
		alias g="genlop -c"
		alias gl="genlop -l"
		alias gt="genlop -t"
	fi

	if cmdExists euse; then alias ei="euse -I"; fi
fi

if cmdExists rc-service; then
	alias r="rc-service"
	alias ru="rc-update"
fi

################################################################################
#                                    Prompt                                    #
################################################################################

#       3-bit 8-bit     24-bit  R G B
BOLD="\[\e[1m\]"
RESET="\[\e[m\]"
UNBOLD="\[\e[22m\]" # TODO

bgOrFg() { if [[ $1 == fg ]]; then printf 3; else printf 4; fi }

color() {
	local d=$(bgOrFg $1)

	if [[ "$COLORTERM" == "truecolor" ]]; then
		printf "\[\e[${d}8;2;$4;$5;$6m\]"
		return
	fi

	printf "\[\e[${d}$2m\e[${d}8;5;$3m\]"
}

case $HOSTNAME in
	hydrogen)          host="5 144 187 154 247" ;;
	helium)            host="2 51 71 141 131"   ;;
	lithium|localhost) host="2 28 0 153 56"     ;;
	*)                 host="7 7 255 255 255"   ;;
esac

case $(whoami) in
root)
	dir="1 1 255 0 0"
	user="1 1 255 0 0"
	priv="1 1 255 0 0"
	;;
waldo)
	dir="6 51 83 167 154"
	user="2 51 96 192 178"
	priv="6 51 115 218 202"
	;;
liesl)
	dir="4 0 255 0 127"
	user="5 144 187 154 247"
	priv="4 4 255 0 127"
	;;
u[0-9]_????)
	dir="2 34 0 178 65"
	user="2 40 0 204 74"
	priv="2 46 0 229 84"
	;;
*)
	user="7 7 255 255 255"
	priv="$user"
	dir="$user"
	;;
esac

# Bash doesn't parse \[ or \] in $PS1 if they're from a string in single quotes,
# so this hacky workaround is neccessary
getExitCode() {
	EXIT_CODE=$?
	if [ $EXIT_CODE != 0 ]; then printf " $EXIT_CODE "; fi
	return $EXIT_CODE
}
getExitPowerline() {
	EXIT_CODE=$?
	if [ $EXIT_CODE != 0 ]; then printf ""; fi
	return $EXIT_CODE
}

segment() { printf "$(color bg $1)$(color fg $BLACK) $2 $RESET$(color fg $1)"; }

RED="1 210 247 118 142"
BLACK="0 0 0 0 0"

exitCode="$(color fg $BLACK)$(color bg $RED)$BOLD"'$(getExitCode)'"$(color fg $RED)$UNBOLD"
host="$(color bg $host)"'$(getExitPowerline)'"$(color fg $BLACK)$(color bg $host) \h $(color fg $host)"
dir="$(segment "$dir" \\w)"
user="$(segment "$user" \\\\u)"
priv="$(segment "$priv" \\$)"
tail="$RESET"

PS1="$exitCode$host$dir$user$priv$tail "

################################################################################
#                                   Startup                                    #
################################################################################

if cmdExists neofetch; then
	neofetch --config none --no_config --cpu_speed on                      \
		--distro_shorthand on --uptime_shorthand tiny --gpu_brand off  \
		--de_version off --disable packages --disable resolution       \
		--disable theme --disable icons --disable term_font            \
		--disable wm_theme --os_arch off --cpu_brand off               \
		--shell_version off
fi

################################################################################
#				    History                                    #
################################################################################

HISTSIZE=999999
HISTCONTROL=erasedups

# Tail
unset -f cmdExists

# config

My Linux configuration files

## [kitty](kitty/) + [bash](bash/)

![Screenshot of a kitty terminal window with neofetch's output followed by a
shell prompt](res/kitty-bash.png)

## [btop](btop/)

![Screenshot of a kitty terminal window with btop's output](res/btop.png)

## [(neo)vim](vim/)

![Screenshot of a kitty terminal window with neovim and some code](res/nvim.png)

## Other

- [linux](linux/)

  My Linux kernel config

- [portage](portage/)

  Files that belong in /etc/portage/ on Gentoo

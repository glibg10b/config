#!/bin/bash

# Absolute path
currentDir=$(cd $(dirname $0) && pwd)

mkdir -p ~/.config/kitty/
cp "$currentDir/kitty.conf" ~/.config/kitty/kitty.conf

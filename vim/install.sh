#!/bin/bash

# Absolute path
currentDir=$(cd $(dirname $0) && pwd)

rm -r ~/.vim/vimrc ~/.config/nvim/init.vim ~/.vim/common-plugins.vim \
	~/.vim/vim.d
mkdir -p ~/.config/nvim/ ~/.vim
cp -r "$currentDir/vimrc" "$currentDir/common-plugins.vim" "$currentDir/vim.d/" \
	~/.vim/
cp "$currentDir/init.vim" ~/.config/nvim/

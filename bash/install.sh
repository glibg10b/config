#!/bin/bash

# Absolute path
currentDir=$(cd $(dirname $0) && pwd)

mv ~/.bashrc ~/.bashrc.bak
cp "$currentDir/.bashrc" ~
